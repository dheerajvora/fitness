import React from 'react'
import AboutUs from '../component/AboutUs'
import Benefits from '../component/Benefits'
import ContactForm from '../component/ContactForm'
import Footer from '../component/Footer'
import Gallery from '../component/Gallery'
import HeroBanner from '../component/HeroBanner'
import MapSection from '../component/MapSection'
import Menu from '../component/Menu'

const Home = () => {
    return (
        <React.Fragment>
            <div className="mobile-banner">
                <HeroBanner
                    img="assets/images/banner.jpg"
                    subtitle="Now pay per use & gym anytime, anywhere!"
                    titleOne="Fitness"
                    titleTwo="Redeemed"
                />
                <Menu />
            </div>
            <AboutUs />
            <Benefits />
            <Gallery />
            <ContactForm />
            <MapSection />
            <Footer />
        </React.Fragment>
    )
}

export default Home
