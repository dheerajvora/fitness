import React from 'react'
import { Navbar, Nav } from 'react-bootstrap'
const Menu = () => {
    return (
        <section className="menu-section container-fluid">
            <Navbar expand="lg">
                <Navbar.Brand href="#home">
                    <img src="assets/images/fitness-logo.png" alt="logo" className="logo"/>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" >
                         <img src="assets/images/bar.svg" alt="logo" className="logo"/>
                </Navbar.Toggle>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto text-uppercase">
                        <Nav.Link href="#home">Home</Nav.Link>
                        <Nav.Link href="#about-fitness">About Fitness</Nav.Link>
                        <Nav.Link href="#benefits">Benefits</Nav.Link>
                        <Nav.Link href="#gallery">Gallery</Nav.Link>
                        <Nav.Link href="#contact">Contact</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </section>
    )
}
export default Menu
