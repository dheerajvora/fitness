import React from 'react'

const MapSection = () => {
    return (
        <section className="map-section">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3502.333707172254!2d77.03275251440694!3d28.619758991411217!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d0fddddc485a9%3A0xb5b805df0e7d930c!2sWebReinvent%20Technologies%20Pvt.%20Ltd.!5e0!3m2!1sen!2sin!4v1607553884521!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style={{border:0}} allowfullscreen="" aria-hidden="false" tabindex="0" title="map of the location"></iframe>
        </section>
    )
}

export default MapSection
