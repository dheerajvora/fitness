import React from 'react'


const HeroBanner = (props) => {
    return (
        <section className="hero-banner position-relative" style={{ backgroundImage: `url('${props.img}')` }}>
            <div className="container">
    <h1 className="title text-primary text-uppercase">{props.titleOne}<span className="d-block text-white">{props.titleTwo}</span></h1>
                <p className="subtitle">{props.subtitle}</p>
            </div>
        </section>
    )
}

export default HeroBanner
