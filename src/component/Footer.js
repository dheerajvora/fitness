import React from 'react'

const Footer = () => {
    return (
        <footer>
            <div className="footer">
                <div className="container-fluid">
                    <div className="row align-items-center">
                        <div className="col-md-6">
                            <ul className="info-link">
                                <li>
                                    <p><img src="assets/images/location.svg" alt="app link" className="img-fluid" /> <span>Lorem ipsum dolor sit amet</span></p>
                                </li>
                                <li>
                                    <p><a href="tel:919898989898"><img src="assets/images/call.svg" alt="app link" className="img-fluid" /> <span>+91 9898989898</span></a></p>
                                </li>
                                <li>
                                    <p><a href="mailto:recomp@fitness.com"><img src="assets/images/mail.svg" alt="app link" className="img-fluid" /> <span>recomp@fitness.com</span></a></p>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-3">
                            <ul className="social-link">
                                <li>
                                    <a href="#home"> <i className="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#home"><i className="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#home"><i className="fa fa-google-plus"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-3 text-center-md">
                            <img src="assets/images/link.png" alt="app link" className="img-fluid" />
                        </div>
                    </div>
                </div>
            </div>
            <div className="copyright">
                <div className="container-fluid">
                    <p className="mb-0 py-3">Copyright © 2018 Fitness All Rights Reserved.</p>
                </div>
            </div>
        </footer>

    )
}

export default Footer
