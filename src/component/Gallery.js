import React from 'react'

const Gallery = () => {
    return (
        <section className="section-padding-top gallery-section">
            <div className="container">
                <div className="heading text-center">
                    <h2 className="title text-uppercase">Gallery</h2>
                </div>
                <div class="text-center">
                    <ul className="p-0 m-0 text-uppercase">
                        <li class="filter" data-filter="all">All</li>
                        <li class="filter" data-filter=".photos">photos</li>
                        <li class="filter" data-filter=".videos">Videos</li>
                    </ul>
                </div>
                <div class="mix-container pt-4">
                    <div className="row zoom-gallery">
                        <div className="col-md-6 pb-4 mix videos">
                            <a class="popup-youtube" href="https://www.youtube.com/embed/0UscoQcmBRI">
                                <div className="video-box">
                                    <img src="assets/images/video.png" alt="fitness"  />
                                    <div className="video-icon" style={{ backgroundImage: `url("assets/images/video-icon.svg")` }}></div>
                                </div>
                            </a>
                        </div>
                        <div className="col-md-3 pb-4 mix photos">
                            <a href="assets/images/img-five.png">
                                <div className="image-box">
                                    <img src="assets/images/img-five.png" alt="fitness"  />
                                    <div className="image-content">
                                       <h4 className="text-uppercase"> Interior Gallery</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div className="col-md-3 pb-4 mix photos">
                            <a href="assets/images/img-two.png">
                                <div className="image-box">
                                    <img src="assets/images/img-two.png" alt="fitness"  />
                                    <div className="image-content">
                                       <h4 className="text-uppercase"> Interior Gallery</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div className="col-md-6 pb-4 mix photos">
                            <a href="assets/images/img-one.png">
                                <div className="image-box">
                                    <img src="assets/images/img-one.png" alt="fitness" />
                                    <div className="image-content">
                                       <h4 className="text-uppercase"> Interior Gallery</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div className="col-md-3 pb-4 mix photos">
                            <a href="assets/images/img-three.png">
                                <div className="image-box">
                                    <img src="assets/images/img-three.png" alt="fitness" />
                                    <div className="image-content">
                                       <h4 className="text-uppercase"> Interior Gallery</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div className="col-md-3 pb-4 mix photos">
                            <a href="assets/images/img-four.png">
                                <div className="image-box">
                                    <img src="assets/images/img-four.png" alt="fitness" />
                                    <div className="image-content">
                                       <h4 className="text-uppercase"> Interior Gallery</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Gallery
