import React from 'react'

const Benefits = () => {
    const benefitsBox = [
        {
            img: "assets/images/state-of-art-equipments.png",
            title: "State Of Art Equipments",
            text: " Modern conveniences & functional spaces to help you gain more from workout",
            link: "#"
        }, {
            img: "assets/images/pay-per-use.png",
            title: "Pay per use",
            text: "Pay only for the number of hours you workout in gym gain more from workout",
            link: "#"
        },
        {
            img: "assets/images/certified-experts.png",
            title: "certified experts",
            text: "Certified experts and trainers to help you acheive your fitness goals",
            link: "#"
        }
    ]
    return (
        <section className="section-padding-top section-margin-top section-padding-bottom benefit-section">
            <div className="container">
                <div className="heading text-center">
                    <h2 className="title text-uppercase">Benefits</h2>
                    <p className="subtitle">We make it easy and affordable for you to stay fit and healthy</p>
                </div>
                <div  className="owl-carousel owl-theme">
                    {
                        benefitsBox.map((item, i) => (
                            <div className="item" key="i">
                                <div className="box text-center bg-white">
                                    <div className="box-image">
                                        <img src={item.img} alt="icon" className="img-fluid" />
                                    </div>
                                    <div className="box-content  pt-5">
                                        <h3>{item.title}</h3>
                                        <p>
                                            {item.text}
                                        </p>
                                        <a href={item.link} className="btn btn-primary">Learn more</a>
                                    </div>
                                </div>

                            </div>
                        ))
                    }
                </div>
            </div>
        </section>
    )
}

export default Benefits
