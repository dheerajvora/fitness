import React, { Component } from 'react'

class ContactForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            message: ""
        }
    }
    inputHandle = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    inputSubmit = (e) => {
        e.preventDefault();
        alert(`Hello ${this.state.name}`)
    }
    render() {
        return (
            <section className="section-padding-top section-margin-top section-padding-bottom contact-section">
                <div className="container">
                    <div className="heading text-center">
                        <h2 className="title text-uppercase">Benefits</h2>
                        <p className="subtitle">Please fill out the quick form and we will be in touch with you at the earliest</p>
                    </div>
                    <form onSubmit={this.inputSubmit}>
                        <div className="row">
                            <div className="col-md-6">
                                <div class="form-group">
                                    <input
                                        type="text"
                                        class="form-control"
                                        name="name"
                                        placeholder="Name"
                                        value={this.state.name}
                                        onChange={this.inputHandle} />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div class="form-group">
                                    <input
                                        type="email"
                                        class="form-control"
                                        name="email"
                                        aria-describedby="emailHelp"
                                        placeholder="Email"
                                        value={this.state.email}
                                        onChange={this.inputHandle}
                                    />
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div class="form-group">
                                    <textarea
                                        type="text"
                                        class="form-control"
                                        name="message"
                                        aria-describedby="emailHelp"
                                        placeholder="Tell us what you are looking for"
                                        value={this.state.message}
                                        onChange={this.inputHandle}
                                    />
                                </div>
                            </div>
                            <div className="col-md-12 pt-2">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-primary " >Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        )
    }

}

export default ContactForm
