import React from 'react'
import SwiperCore, { EffectFade,Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.scss';
import 'swiper/components/effect-fade/effect-fade.scss';
import 'swiper/components/navigation/navigation.scss';
SwiperCore.use([EffectFade,Navigation]);

const AboutUs = () => {

    const sliderValue = [
        {
            img: "assets/images/person-one.png", text: `“Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since
            the 1500s, when an unknown printer took a galley of type and scrambled
        it to make a type specimen book. It has survived not only five centuries”`, name: "Rohit Chattopadhyay",
            socialLink: [
                { icon: "facebook", link: "#" },
                { icon: "twitter", link: "#" }
            ]
        },
        {
            img: "assets/images/person-two.png", text: `“It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.”`, name: "Dheeraj Vora",
            socialLink: [
                { icon: "facebook", link: "#" },
                { icon: "twitter", link: "#" },
                { icon: "instagram", link: "#" }
            ]
        },
        {
            img: "assets/images/person-three.png", text: `“Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words”`, name: "UI Developer",
            socialLink: [
                { icon: "facebook", link: "#" },
                { icon: "instagram", link: "#" },
                { icon: "twitter", link: "#" },
                { icon: "google", link: "#" }
            ]
        }
    ]
    return (
        <section className="section-padding-top about-us-section">
            <div className="heading text-center">
                <p className="subtitle">A word from our Creators</p>
                <h2 className="title text-uppercase">About Fitness</h2>
            </div>
            <Swiper
                effect="fade"
                navigation
                slidesPerView={1}
                onSlideChange={() => console.log('slide change')}
                onSwiper={(swiper) => console.log(swiper)}
            >
                {
                    sliderValue.map((num, i) => (
                        <SwiperSlide key="i">
                            <div className="w-100">
                                <div className="row">
                                    <div className="col-md-6 bg-light text-center">
                                        <div className="pt-4">
                                            <img src={num.img} alt="slider person" />
                                        </div>
                                    </div>
                                    <div className="col-md-6 bg-primary">
                                        <div className="text-container text-center text-white" style={{ backgroundImage: `url("assets/images/quotes.png")` }}>
                                            <p>{num.text}</p>
                                            <span>{num.name}</span>
                                            <ul className="social-link">
                                                {
                                                    num.socialLink.map((link, i) => (
                                                        <li key="i">
                                                            <a href={link.link}><i className={`fa fa-${link.icon}`}></i></a>
                                                        </li>
                                                    ))
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </SwiperSlide>
                    ))
                }
            </Swiper>
        </section>
    )
}

export default AboutUs
